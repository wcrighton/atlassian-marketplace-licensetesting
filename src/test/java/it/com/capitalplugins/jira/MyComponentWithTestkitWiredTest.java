package it.com.capitalplugins.jira;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.exception.CreateException;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.rest.api.issue.IssueCreateResponse;
import com.atlassian.jira.testkit.client.Backdoor;
//import com.atlassian.jira.testkit.client.restclient.Issue;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.testkit.client.util.TestKitLocalEnvironmentData;
import com.atlassian.jira.testkit.client.util.TimeBombLicence;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.usercompatibility.UserManager;
import com.atlassian.sal.usercompatibility.UserProfile;
import com.capitalplugins.jira.MyPluginComponent;
import com.capitalplugins.jira.utils.LocalTestPluginUtils;
import com.capitalplugins.jira.utils.LocalTestPluginUtilsImpl;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.*;

import static org.junit.Assert.*;
//import com.atlassian.maven.plugins.amps.Product;

@RunWith( AtlassianPluginsTestRunner.class )
public class MyComponentWithTestkitWiredTest
{
    private final ApplicationProperties applicationProperties;
    private final MyPluginComponent     myPluginComponent;
    private final PluginAccessor        pluginAccessor;
    private final LocalTestPluginUtils  localTestUtils;

    Logger log = Logger.getLogger(MyComponentWithTestkitWiredTest.class);

    public MyComponentWithTestkitWiredTest( ApplicationProperties applicationProperties
                                           , MyPluginComponent myPluginComponent
                                           , PluginAccessor pluginAccessor )
    {
        this.applicationProperties = applicationProperties;
        this.myPluginComponent = myPluginComponent;
        this.pluginAccessor = pluginAccessor;
        this.localTestUtils = new LocalTestPluginUtilsImpl();
    }


    @Test
    public void setUp()
    {
        Backdoor testKit = new Backdoor(new TestKitLocalEnvironmentData());
        //testKit.restoreBlankInstance(TimeBombLicence.LICENCE_FOR_TESTING);
        testKit.usersAndGroups().addUser("newuser1", "admin", "Child ofTestkit", "test1@capitalcityconsultants.com");

        testKit.websudo().disable();
        testKit.subtask().enable();

        log.debug("Creating a Last Comment custom field");
        String customFieldId = testKit.customFields().createCustomField("Last Comment", "Description of the Last Comment Custom Field", "type", "key");
        assertNotNull("Unable to create Last Comment custom field", customFieldId);

        log.debug("Creating a new Project");
        testKit.project().addProject("Killing over Losing", "TKOL", "admin");

        log.debug("Creating a new issue in the new project");
        IssueCreateResponse issueCreateResponse = testKit.issues().createIssue("TKOL", "Total Killing Over Losing", "newuser1");
        assertNotNull("FAilure creating new issue in the new TKOL project", issueCreateResponse);

        log.debug("Pulling the value of the last Comment custom field from the new issue in the new project...");

        assertNotNull("Unable to retrieve the new issue from the new project", testKit.issues().getIssue(issueCreateResponse.key));

        log.debug("Adding a comment to our testIssue in the new project");
        testKit.issues().commentIssue(issueCreateResponse.key, "Here is a comment added to our new issue");
    }

    @Test
    public void testMyName()
    {
        assertEquals("names do not match!", "myComponent:" + applicationProperties.getDisplayName(), myPluginComponent.getName());
    }

    @Test
    public void testMyPluginInformation()
    {
        String intel = myPluginComponent.getMyPluginInformation();

        log.debug("Here is the plugin intel: \n" + intel);

        assertNotNull("Testing if we are able to access myPlugin's getInformation type method", intel);
    }

    @Test
    public void shouldDoSomething()
    {
        assertTrue("This is an example of us doing something undefined", 1 == 1);
    }

    @Test
    public void testMyPluginInjection()
    {
        assertNotNull("This is testing that the myPlugin object dependency injection thingy worked", myPluginComponent);
    }

}

