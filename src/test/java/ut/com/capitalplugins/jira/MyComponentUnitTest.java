package ut.com.capitalplugins.jira;

import com.capitalplugins.jira.MyPluginComponent;
import com.capitalplugins.jira.MyPluginComponentImpl;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent", component.getName());
    }

    @Ignore
    @Test
    public void testAdd()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("Result", 8, component.addNumbers(8, 8));
    }

}
